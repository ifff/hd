with (import <nixpkgs> {});
stdenv.mkDerivation rec {
   name = "hd";
   buildInputs = [
     gcc
     gnumake
     libsixel
   ];
}
